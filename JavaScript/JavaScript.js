window.addEventListener('load', ()=> {
    let long;
    let lat;
    let temperatureDesctiption = document.querySelector('.temperature-description');
    let temperatureDegree = document.querySelector('.temperature-degree');
    let locationTimezone = document.querySelector('.location-timezone');
    const temperatureSpan = document.querySelector('.temperature span');


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;

            const proxy = 'https://cors-anywhere.herokuapp.com/';

            const api = `${proxy}https://api.darksky.net/forecast/3e4d50d408a48818bd746b17e08d7521/${lat},${long}?units=si`;
            fetch(api)
                .then(response => {
                    return response.json()
                })
                .then( data => {
                    const {temperature, summary, icon} = data.currently;
                    let temp = Math.floor(temperature);

                    temperatureDegree.textContent = temp;
                    temperatureDesctiption.textContent = summary;
                    locationTimezone.textContent = data.timezone;

                    setIcons(icon, document.querySelector('.icon'));
                    // Formula for celsius
                    // let celsius = (temperature - 32)*(5/9);
                    let fahrenheit =(temp * 9/5 + 32);

                    // Change temperature to Celsius/Fahrenheit
                    temperatureDegree.addEventListener('click', () => {
                        if (temperatureSpan.textContent === 'C'){
                            temperatureSpan.textContent = 'F';
                            temperatureDegree.textContent = Math.floor(fahrenheit).toString()
                        } else {
                            temperatureSpan.textContent = "C";
                            temperatureDegree.textContent = temp.toString()

                            // temperatureDegree.textContent = Math.floor(fahrenheit)
                        }
                    })

                });
        });
    }
    function setIcons(icon, iconID) {
        const skycons = new Skycons({color: 'white'});
        const currentIcon = icon.replace(/-/g, "_").toUpperCase();
        skycons.play();
        return skycons.set(iconID, Skycons[currentIcon])
    }
});